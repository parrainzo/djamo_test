
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:postapp/core/error/failure.dart';
import 'package:postapp/core/usecases/usecase.dart';
import 'package:postapp/features/posts/domain/entities/posts.dart';
import 'package:postapp/features/posts/domain/repositories/posts_repository.dart';

class AddPost extends UseCase<Post, Params>{
  late final PostsRepository postsRepository;

  AddPost(this.postsRepository);

  @override
  Future<Either<Failure, Post>?> call(Params params) async{
    return await postsRepository.addPost(title: params.title, body: params.body);
  }

}

class Params extends Equatable {
  final String title;
  final String body;

  Params({required this.title, required this.body}) : super();

  @override
  // TODO: implement props
  List<Object?> get props => [title, body];
}