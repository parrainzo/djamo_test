
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:postapp/core/error/failure.dart';
import 'package:postapp/core/usecases/usecase.dart';
import 'package:postapp/features/posts/domain/entities/posts.dart';
import 'package:postapp/features/posts/domain/repositories/posts_repository.dart';

class GetPosts extends UseCase<List<Post>, NoParams>{
  late final PostsRepository postsRepository;

  GetPosts(this.postsRepository);

  @override
  Future<Either<Failure, List<Post>>?>? call(NoParams params) async {
    return await postsRepository.getAllPosts();
  }
}