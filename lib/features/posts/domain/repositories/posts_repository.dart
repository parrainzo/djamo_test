

import 'package:dartz/dartz.dart';
import 'package:postapp/core/error/failure.dart';
import 'package:postapp/features/posts/domain/entities/posts.dart';

abstract class PostsRepository {
  Future<Either<Failure, List<Post>>>? getAllPosts();
  Future<Either<Failure, Post>>? addPost({String title, String body});
}