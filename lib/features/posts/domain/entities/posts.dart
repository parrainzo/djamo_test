import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Post extends Equatable {
  final int? userId;
  final int? id;
  final String? title;
  final String? body;


  Post({
    required this.userId,
    required this.id,
    required this.title,
    required this.body,
  }) : super();

  @override
  // TODO: implement props
  List<Object?> get props => [userId, id, title, body];
}