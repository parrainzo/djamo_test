
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:postapp/core/usecases/usecase.dart';
import 'package:postapp/features/posts/domain/entities/posts.dart';
import 'package:postapp/features/posts/domain/repositories/posts_repository.dart';
import 'package:postapp/features/posts/domain/usecases/get_all_posts.dart';

class MockPostsRepository extends Mock
    implements PostsRepository {}

void main() {
  late GetPosts useCase;
  late MockPostsRepository mockPostsRepository;

  setUp(() {
    mockPostsRepository = MockPostsRepository();
    useCase = GetPosts(mockPostsRepository);
  });

  final postList = [
    Post(id: 1, userId: 1, title: 'Title', body: 'Body'),
    Post(id: 2, userId: 2, title: 'Title2', body: 'Body2')
  ];

  test(
    'should get all posts from the repository',
        () async {
      // "On the fly" implementation of the Repository using the Mockito package.
      // When getConcreteNumberTrivia is called with any argument, always answer with
      // the Right "side" of Either containing a test NumberTrivia object.
      when(mockPostsRepository.getAllPosts())
          .thenAnswer((_) async => Right(postList));
      // The "act" phase of the test. Call the not-yet-existent method.
      final result = await useCase(NoParams());
      // UseCase should simply return whatever was returned from the Repository
      expect(result, Right(postList));
      // Verify that the method has been called on the Repository
      verify(mockPostsRepository.getAllPosts());
      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockPostsRepository);
    },
  );
}